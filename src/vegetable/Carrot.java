package vegetable;
/**
 *
 * @author mitpatel
 */
public class Carrot extends Vegetables 
{
    //constructor
    public Carrot(String colour, double size) 
    {
        super(colour, size);
    }

    public void isRipe() 
    {
        if(getColour().equalsIgnoreCase("orange") && getSize()==1.5) {
            System.out.println("carrot is ripe");
        }
    
    }
}

