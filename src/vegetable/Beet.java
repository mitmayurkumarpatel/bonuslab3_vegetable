package vegetable;
/**
 *
 * @author mitpatel
 */
public class Beet extends Vegetables 
{
 
    public Beet(String colour, double size) {
        super(colour, size);
    }

    public void isRipe() {
        if(getColour().equalsIgnoreCase("Red") && getSize()==2) {
            System.out.println("Beet is ripe");
        }
    
    }
}